package DB_Cassandra

import org.apache.spark.sql._
import org.apache.spark.sql.cassandra._

import com.datastax.spark.connector.cql.CassandraConnectorConf

object restaurants extends App {
  // ***********************
  // Initialisation de Spark
  // ***********************

  // Point d'entrée Spark SQL: SparkSession
  val sparkSession = SparkSession
    .builder()
    .appName("Restaurants et Inspections")
    .master("local")
    .config("spark.cassandra.connection.host", "127.0.0.1")
    .config("spark.cassandra.connection.port", "9160")
    .getOrCreate()

  // *********************
  // Connexion à Cassandra
  // *********************
  // Paramètres globaux pour tous les clusters and keyspaces
  sparkSession.setCassandraConf(CassandraConnectorConf.KeepAliveMillisParam.option(10000))

  // Paramètre pour un cluster spécifique
  sparkSession.setCassandraConf("Cluster1", CassandraConnectorConf.ConnectionHostParam.option("127.0.0.1")
    ++ CassandraConnectorConf.ConnectionPortParam.option(9160))

  // ************************************************************
  // Lecture des tables dans Cassandra et création des DataFrames
  // ************************************************************
  val restaurantsDF = sparkSession.read
    .format("org.apache.spark.sql.cassandra")
    .options(Map("keyspace" -> "resto_ny", "table" -> "restaurant"))
    .load()
  restaurantsDF.printSchema()
  //restaurantsDF.show()

  val inspectionsDF = sparkSession.read
    .format("org.apache.spark.sql.cassandra")
    .options(Map("keyspace" -> "resto_ny", "table" -> "inspection"))
    .load()
  inspectionsDF.printSchema()
  inspectionsDF.show()

  // ************
  // Requêtes SQL
  // ************
  // Enregistrement de la DataFrame en vu temporaire
  restaurantsDF.createOrReplaceTempView("restaurant")
  inspectionsDF.createOrReplaceTempView("inspection")

  // 1. Liste de tous les restaurants.
  sparkSession.sql("select * from restaurant").show()

  // 2. Liste des Noms de restaurants.
  //sparkSession.sql("select name from restaurant").show()

  // 3. Nom et quartier (borough) du restaurant N° 41569764.
  //sparkSession.sql("select name, borough from restaurant where id = 41569764").show()

  // 4. Dates et grades des inspections de ce restaurant.
  //sparkSession.sql("select inspectiondate, grade from inspection where idrestaurant = 41569764").show()

  // 5. Noms des restaurants de cuisine Française (French).
  //sparkSession.sql("select name from restaurant where cuisinetype = 'French'").show()

  // 6. Noms des restaurants situés dans BROOKLYN (attribut borough).
  //sparkSession.sql("select name, borough from restaurant where borough = 'BROOKLYN'").show()

  // 7. Grades et scores donnés pour une inspection pour le restaurant n° 41569764 avec un score d’au moins 10.
/*  sparkSession.sql(
    """
      select grade, score from inspection
      where idrestaurant = 41569764 and score >= 10
    """.stripMargin).show()*/

  // 8. Grades (non nuls) des inspections dont le score est supérieur à 30.
  // sparkSession.sql("select grade, score from inspection where grade is not null and score > 30").show()

  // 9. Nombre de lignes retournées par la requête précédente.
  // sparkSession.sql("select count(*) from inspection where grade is not null and score > 30").show()

  //  10. Grades des inspections dont l’identifiant est compris
  //  entre 40 000 000 et 41 000 000.
  /*sparkSession.sql(
    """
      |select idrestaurant, grade from inspection
      |where idrestaurant >= 40000000  and idrestaurant <= 41000000
    """.stripMargin).show()*/

  // 11. Compter le nombre de lignes retournées par la requête précédente.
  //sparkSession.sql("select count(*) from inspection where idrestaurant >= 40000000 and idrestaurant < 41000000").show()

  // 12. SELECT Name FROM Restaurant WHERE borough='BROOKLYN' ;
  // Dans cqlsh taper créer un index :
  //
  // cqlsh:resto_ny> create index idx_borough on restaurant(borough);
  //
  //sparkSession.sql("select name, borough from restaurant where borough = 'BROOKLYN'").show()

  // 13. Utilisons les deux index sur Restaurant (borough et cuisineType).
  // Trouvez tous les noms de restaurants français de Brooklyn.
  //sparkSession.sql("select name,  borough, cuisinetype from restaurant where cuisinetype = 'French' and borough = 'BROOKLYN'").show()

  // 15. On veut les noms des restaurants ayant au moins un grade ‘A’ dans leurs inspections (Impossible dans
  // Cassandra, mais possible dans Spark!!!
  sparkSession.sql("select distinct(grade) from inspection").show() // grades disponibles
  sparkSession.sql(
    """
      |select distinct(restaurant.id), restaurant.name, restaurant.borough, restaurant.street, restaurant.zipcode, inspection.grade
      |from restaurant left outer join inspection
      |where restaurant.id = inspection.idrestaurant and inspection.grade = 'A'
    """.stripMargin).show()

  // ou bien avec l'API DataFrame
  restaurantsDF.join(inspectionsDF, restaurantsDF("id") === inspectionsDF("idrestaurant"), "left_outer")
      .where(inspectionsDF("grade") === "A").select("id", "name", "borough", "street", "grade")
    .show()
}
