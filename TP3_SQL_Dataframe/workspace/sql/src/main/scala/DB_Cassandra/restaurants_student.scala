package DB_Cassandra

import org.apache.spark.sql._
import org.apache.spark.sql.cassandra._

import com.datastax.spark.connector.cql.CassandraConnectorConf

object restaurants_student extends App {
  // ***********************
  // Initialisation de Spark
  // ***********************

  // Point d'entrée Spark SQL: SparkSession
  val sparkSession = SparkSession
    .builder()
    .appName("Restaurants et Inspections")
    .master("local")
    .config("spark.cassandra.connection.host", "192.168.99.100")
    .config("spark.cassandra.connection.port", "32769")
    .getOrCreate()

  // *********************
  // Connexion à Cassandra
  // *********************
  // Paramètres globaux pour tous les clusters and keyspaces
  sparkSession.setCassandraConf(
  CassandraConnectorConf.KeepAliveMillisParam.option(10000))

  // Paramètre pour un cluster spécifique
  sparkSession.setCassandraConf("Cluster1",
  CassandraConnectorConf.ConnectionHostParam.option("192.168.99.100")
    ++ CassandraConnectorConf.ConnectionPortParam.option(32769))

  // ************************************************************
  // Lecture des tables dans Cassandra et création des DataFrames
  // ************************************************************
  val restaurantsDF = sparkSession.read
    .format("org.apache.spark.sql.cassandra")
    .options(Map("keyspace" -> "resto_ny", "table" -> "restaurant"))
    .load()
  restaurantsDF.printSchema()
  restaurantsDF.show()

  val inspectionsDF = sparkSession.read
    .format("org.apache.spark.sql.cassandra")
    .options(Map("keyspace" -> "resto_ny", "table" -> "inspection"))
    .load()
  inspectionsDF.printSchema()
  inspectionsDF.show()

  // ************
  // Requêtes SQL
  // ************
  // Enregistrement de la DataFrame en vue temporaire
  restaurantsDF.createOrReplaceTempView("restaurant")
  inspectionsDF.createOrReplaceTempView("inspection")

  // 1. Liste de tous les restaurants.
  //???

  // 2. Liste des Noms de restaurants.
  //???

  // 3. Nom et quartier (borough) du restaurant N° 41569764.
  //???

  // 4. Dates et grades des inspections de ce restaurant.
  //???

  // 5. Noms des restaurants de cuisine Française (French).
  //???

  // 6. Noms des restaurants situés dans BROOKLYN (attribut borough).
  //???

  // 7. Grades et scores donnés pour une inspection pour le restaurant n° 41569764 avec un score d’au moins 10.
  //???

  // 8. Grades (non nuls) des inspections dont le score est supérieur à 30.
  //???

  // 9. Nombre de lignes retournées par la requête précédente.
  //???

  //  10. Grades des inspections dont l’identifiant est compris
  //  entre 40 000 000 et 41 000 000.
  //???

  // 11. Compter le nombre de lignes retournées par la requête précédente.
  //???

  // 12. SELECT Name FROM Restaurant WHERE borough='BROOKLYN' ;
  // Dans cqlsh taper créer un index :
  //
  // cqlsh:resto_ny> create index idx_borough on restaurant(borough);
  //
  //???

  // 13. Utilisons les deux index sur Restaurant (borough et cuisineType).
  // Trouvez tous les noms de restaurants français de Brooklyn.
  //???

  // 15. On veut les noms des restaurants ayant au moins un grade ‘A’ dans leurs inspections (Impossible dans
  // Cassandra, mais possible dans Spark!!!
  sparkSession.sql("select distinct(grade) from inspection").show() // grades disponibles
  //???

  // ou bien avec l'API DataFrame
  //restaurantsDF.???
}
