package exemples

import exemples.dataframes.sc
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql._

object jointures_dataframes extends App {
  // Données
  val clientsDB = List(
    (100, "John DOE"),
    (101, "Jane DOE"),
    (102, "Clark KENT")
  )

  val articlesDB = List(
    (100, "Lait"),
    (100, "Deodorant"),
    (101, "Crème de jour"),
    (101, "Book"),
    (101, "Lunettes"),
    (103, "Super PC")
  )

  // Initialisation de Spark avec un SparkContext
  val appName = "DataFrames et SQL"
  val master = "local"
  val conf = new SparkConf().setAppName(appName).setMaster(master)
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")

  // Point d'entrée Spark SQL: SparkSession
  val spark = SparkSession
    .builder()
    .appName("Spark SQL basic example")
    .config("spark.some.config.option", "some-value")
    .getOrCreate()

  // Pour la conversion des RDDs en DataFrames (.toDF)
  import spark.implicits._

  // Chargement des données dans un RDD
  val clientsRDD = sc.parallelize(clientsDB)
  val articlesRDD = sc.parallelize(articlesDB)

  // ***********************
  // Création des DataFrames
  // ***********************
  val clientsDF = clientsRDD.toDF("id", "nom")
  clientsDF.show()
  //  +---+----------+
  //  | id|       nom|
  //  +---+----------+
  //  |100|  John DOE|
  //  |101|  Jane DOE|
  //  |102|Clark KENT|
  //  +---+----------+
  clientsDF.printSchema()
  //  root
  //  |-- id: integer (nullable = true)
  //  |-- nom: string (nullable = true)

  val articlesDF = articlesRDD.toDF("id_client", "article")
  articlesDF.show()
  //  +---------+-------------+
  //  |id_client|      article|
  //  +---------+-------------+
  //  |      100|         Lait|
  //  |      100|    Deodorant|
  //  |      101|Crème de jour|
  //  |      101|         Book|
  //  |      101|     Lunettes|
  //  |      103|     Super PC|
  //  +---------+-------------+
  articlesDF.printSchema()
  //  root
  //  |-- id_client: integer (nullable = true)
  //  |-- article: string (nullable = true)

  // ****************************
  // Jointures sur les DataFrames
  // ****************************

  // Inner join
  val joined = clientsDF.join(articlesDF,$"id" === $"id_client")
  joined.show()
  //  +---+--------+---------+-------------+
  //  | id|     nom|id_client|      article|
  //  +---+--------+---------+-------------+
  //  |101|Jane DOE|      101|Crème de jour|
  //  |101|Jane DOE|      101|         Book|
  //  |101|Jane DOE|      101|     Lunettes|
  //  |100|John DOE|      100|         Lait|
  //  |100|John DOE|      100|    Deodorant|
  //  +---+--------+---------+-------------+

  // Left outer join
  val leftJoin = clientsDF.join(articlesDF,$"id" === $"id_client", "left_outer")
  leftJoin.show()
  //  +---+----------+---------+-------------+
  //  | id|       nom|id_client|      article|
  //  +---+----------+---------+-------------+
  //  |101|  Jane DOE|      101|Crème de jour|
  //  |101|  Jane DOE|      101|         Book|
  //  |101|  Jane DOE|      101|     Lunettes|
  //  |100|  John DOE|      100|         Lait|
  //  |100|  John DOE|      100|    Deodorant|
  //  |102|Clark KENT|     null|         null|

  // Right outer join
  val rightJoin = clientsDF.join(articlesDF,$"id" === $"id_client", "right_outer")
  rightJoin.show()
  //  +----+--------+---------+-------------+
  //  |  id|     nom|id_client|      article|
  //  +----+--------+---------+-------------+
  //  | 101|Jane DOE|      101|Crème de jour|
  //  | 101|Jane DOE|      101|         Book|
  //  | 101|Jane DOE|      101|     Lunettes|
  //  |null|    null|      103|     Super PC|
  //  | 100|John DOE|      100|         Lait|
  //  | 100|John DOE|      100|    Deodorant|
  //  +----+--------+---------+-------------+

  // Outer join
  val outerJoin = clientsDF.join(articlesDF,$"id" === $"id_client", "outer")
  outerJoin.show()
  //  +----+----------+---------+-------------+
  //  |  id|       nom|id_client|      article|
  //  +----+----------+---------+-------------+
  //  | 101|  Jane DOE|      101|Crème de jour|
  //  | 101|  Jane DOE|      101|         Book|
  //  | 101|  Jane DOE|      101|     Lunettes|
  //  |null|      null|      103|     Super PC|
  //  | 100|  John DOE|      100|         Lait|
  //  | 100|  John DOE|      100|    Deodorant|
  //  | 102|Clark KENT|     null|         null|
  //  +----+----------+---------+-------------+

  // Leftsemi join
  val leftsemiJoin = clientsDF.join(articlesDF,$"id" === $"id_client", "leftsemi")
  leftsemiJoin.show()
  //  +---+--------+
  //  | id|     nom|
  //  +---+--------+
  //  |101|Jane DOE|
  //  |100|John DOE|
  //  +---+--------+
}
