package exemples

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql._
import org.apache.spark.sql.types._


object dataframes extends App {
  // Données: (id, prenom, nom, age)
  val data = Array(
    (100, "John", "DOE", 35),
    (101, "Jane", "DOE", 35),
    (102, "Clark", "KENT", 2000),
    (103, "Bruce", "WAYNE", 70),
    (104, "Peter", "PARKER", 2)
  )

  // Initialisation de Spark avec un SparkContext
  val appName = "Dataframes" // nom de l'application
  val master = "local" // noeud maitre
  val conf = new SparkConf().setAppName(appName).setMaster(master)
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")

  // Point d'entrée Spark SQL: SparkSession
  val spark = SparkSession
    .builder()
    .appName("Spark SQL basic example")
    .config("spark.some.config.option", "some-value")
    .getOrCreate()

  // Pour la conversion des RDDs en DataFrames (.toDF)
  import spark.implicits._

  // Chargement des données dans un RDD
  val dataRDD = sc.parallelize(data)
  dataRDD.foreach(println)

  // **********************************************************
  // Création d'un Dataframe avec inférence de schéma par Spark
  // **********************************************************
  val dataDF1 = dataRDD.toDF("id", "prenom", "nom", "age") // nom de colonnes
  //val dataDF1 = dataRDD.toDF() // avec nom de colonnes numérotées automatiquement
  dataDF1.show() // Affichage des lignes de la DataFrame
  dataDF1.printSchema() // Affichage du schéma de la DataFrame inféré par Spark

  // *********************************************
  // Création d'un DataFrame avec schéma prédéfini
  // *********************************************

  // Définition du schéma de la DataFrame
  val schema = StructType(
    Array(
      StructField("id_2", IntegerType),
      StructField("prenom_2", StringType),
      StructField("nom_2", StringType),
      StructField("age_2", IntegerType)
    )
  )

  // Conversion du RDD en RDD[Row]
  val rowRDD = dataRDD
    .map {
      case (id, prenom, nom, age) =>
        Row(id.toInt, prenom.toString, nom.toString, age.toInt)
    }

  // Application du schéma au RDD[Row]
  val dataDF2 = spark.createDataFrame(rowRDD, schema)
  dataDF2.show()
  dataDF2.printSchema()

  // **************************************
  // Requêtes SQL sur DataFrame (Spark SQL)
  // **************************************

  // Enregistrement de la DataFrame en vue temporaire
  dataDF1.createOrReplaceTempView("personnes")

  // Affichage de colonnes spécifiques
  spark.sql(
    """
      select nom, prenom from personnes order by age
    """.stripMargin).show()

  // Séléction des personnes dont l'âge est inférieur à 10 ans et supérieur à 40 ans
  spark.sql(
    """
      select * from personnes
      where age < 10 or age > 40
    """.stripMargin).show()

  // Regroupement du nombre de personnes par âge
  spark.sql(
    """
      select age, count(age) from personnes group by age
    """.stripMargin).show()


  // ******************************************
  // Requêtes SQL sur DataFrame (DataFrame API)
  // ******************************************

  // Affichage de colonnes spécifiques
  dataDF1.select($"prenom", $"nom").show()

  // Séléction des personnes dont l'âge est inférieur à 10 ans et supérieur à 40 ans
  dataDF1.filter($"age" < 10 || $"age" > 40).show()

  // Regroupement du nombre de personnes par âge
  dataDF1.groupBy("age").count().show()
}
