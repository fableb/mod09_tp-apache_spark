package DB_MongoDB

import com.mongodb.spark.MongoSpark
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.functions._

case class Person(_id: String, last_name: String, first_name: String, birth_date: String)
case class Movie(_id: String, title: String, year: Int, genre: String, summary: String, country: String, director: Person, actors: Array[Person])

object movies extends App {
  // ***********************
  // Initialisation de Spark
  // ***********************

  // Point d'entrée Spark SQL: SparkSession
  val sparkSession = SparkSession
    .builder()
    .appName("Movies")
    .master("local")
    .config("spark.mongodb.input.uri", "mongodb://192.168.99.100:32776/test.movies")
    .config("spark.mongodb.output.uri", "mongodb://192.168.99.100:32776/test.movies")
    .getOrCreate()

  import com.mongodb.spark._
  import com.mongodb.spark.config._
  import org.bson.Document

  // *******************
  // Connexion à MongoDB
  // *******************
  val moviesDF = MongoSpark.load[Movie](sparkSession) // chargement avec le schéma explicite
  moviesDF.printSchema()
  moviesDF.show()

  // ************
  // Requêtes SQL
  // ************
  moviesDF.createOrReplaceTempView("movies")

  sparkSession.sql("select * from movies where title = 'Vertigo'").show()
  sparkSession.sql("select * from movies where title like '%Matrix%'").show()
  sparkSession.sql("select title, actors from movies").show()

  // Extraction des acteurs
  //moviesDF.select(moviesDF("title"), explode(moviesDF("actors")).alias("actor")).show()
  val actorsExtraction = sparkSession.sql("select _id as movie_id, explode(actors) as actor from movies")
  actorsExtraction.printSchema()
  actorsExtraction.show()

  // Creation d'une table actors
  val actorsDF = actorsExtraction.select("movie_id" ,"actor.first_name","actor.last_name", "actor.birth_date")
  actorsDF.printSchema()
  actorsDF.show()
  actorsDF.createOrReplaceTempView("actors")

  // Recherche des films où a jouer Keanu Reeves
  sparkSession.sql("select * from movies, actors where movies._id == actors.movie_id and actors.last_name = 'Reeves'")
    .show()


}
