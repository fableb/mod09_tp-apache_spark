package restaurants

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.cassandra._
import com.datastax.spark.connector.cql.CassandraConnectorConf
import com.datastax.spark.connector.rdd.ReadConf
import exemples.dataframes.sc
import exemples.jointures_dataframes.{articlesDF, clientsDF}

object restaurants extends App {
  // Initialisation de Spark avec un SparkContext
  val appName = "DataFrames et SQL"
  val master = "local"
  val conf = new SparkConf().setAppName(appName).setMaster(master)
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")

  // Point d'entrée Spark SQL: SparkSession
  val sparkSession = SparkSession
    .builder()
    .appName("Restaurants et Inspections")
    .config(conf = conf)
    .getOrCreate()

  // Pour la conversion des RDDs en DataFrames (.toDF)
  import sparkSession.implicits._

  val schema = new StructType()
 //   .add(StructField("id", StringType, true))
 //   .add(StructField("val", IntegerType, true))

  val restaurantsPath = this.getClass.getClassLoader.getResource("restaurants/restaurants.csv").toURI.getPath
  val inspectionsPath = this.getClass.getClassLoader.getResource("restaurants/restaurants_inspections.csv").toURI.getPath
  val restaurantsDF =  sparkSession.read.option("header", "true").csv(restaurantsPath) // à partir de Spark 2.0
  restaurantsDF.printSchema()
  restaurantsDF.show(10)
  val inspectionsDF = sparkSession.read.option("header", "true").csv(inspectionsPath) // à partir de Spark 2.0
  inspectionsDF.printSchema()
  inspectionsDF.show(10)

  // Nombre de restaurants de type cuisine francaise
  val numRestosFrench = restaurantsDF.select($"CuisineType" === "French").count()
  println("Nombre de restos de cuisine francaise :" + numRestosFrench)

  val numGradeA = restaurantsDF.join(inspectionsDF,$"id" === $"idRestaurant")
    .filter($"grade" === "A")
    .count()
  println("Nombre de restaurants ayant un grade 'A' = " + numGradeA)

  println("Noms des restaurants ayant un grade 'A'")
  restaurantsDF.join(inspectionsDF,$"id" === $"idRestaurant")
    .filter($"grade" === "A")
    .select($"Name")
    .show()
}
