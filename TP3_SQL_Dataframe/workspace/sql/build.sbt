name := "sql"

version := "0.1"

scalaVersion := "2.11.8"

scalacOptions ++= Seq("-deprecation")

resolvers += "Spark Packages Repo" at "https://dl.bintray.com/spark-packages/maven"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.1.0",
  "org.apache.spark" %% "spark-sql" % "2.1.0",
  "datastax" % "spark-cassandra-connector" % "2.0.1-s_2.11",
  "org.mongodb.spark" %% "mongo-spark-connector" % "2.2.0",
  "junit" % "junit" % "4.10" % "test"
)

        
