name := "wikipedia"

version := "0.1"

scalaVersion := "2.11.8"

scalacOptions ++= Seq("-deprecation")

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.1.0",
  "org.apache.spark" %% "spark-sql" % "2.1.0",
  "junit" % "junit" % "4.10" % "test"
)