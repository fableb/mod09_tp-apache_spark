# MOD09-TP Apache Spark

### Description

Travaux pratiques de la formation **Apache Spark (Applications avec Scala)** :

### Auteur

2017-, Fabrice LEBEL, fabrice.lebel.pro@outlook.com, [LinkedIn](https://www.linkedin.com/in/fabrice-lebel-b850674?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbqGekl60S9W3uZDa6jyWkg%3D%3D)

### Repo

https://bitbucket.org/fableb/mod09_tp-apache_spark/src/master
