import Joins.sc
import org.apache.spark.{SparkConf, SparkContext}

object shufflingEx2 extends App {
  // Soit un datset du type List(idClient, (fruit, montant)).
  // On désire calculer le montant total dépensé par chaque client en fruits.
  val paniers = Array(
    (100, ("pomme", 2)),
    (100, ("poire", 4)),
    (102, ("poire", 5)),

    (101, ("pomme", 3)),
    (101, ("prune", 5)),
    (102, ("poire", 5)),

    (102, ("orange", 5)),
    (100, ("orange", 10)),
    (100, ("prune", 2))
  )

  // Initialisation de Spark
  val appName = "map values" // nom de l'application
  val master = "local" // noeud maitre
  val conf = new SparkConf().setAppName(appName).setMaster(master)
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")

  // Création d'un RDD clé-valeur
  println("---------------")
  println("Paniers")
  println("---------------")
  val paniersRDD = sc.parallelize(paniers)
  paniersRDD.collect().foreach(println)
  println("---------------")
  println("(id, montant)")
  println("---------------")
  val depenses = paniersRDD.map(v => (v._1,v._2._2))
  depenses.collect().foreach(println)
  println("---------------")
  println("Dépense total par consommateur")
  println("---------------")
  val depenseTotale = depenses.reduceByKey(_+_)
  println(depenseTotale.toDebugString)
  depenseTotale.collect().foreach(println)
}
