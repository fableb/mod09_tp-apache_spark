import org.apache.spark.{SparkConf, SparkContext}

object Joins extends App {
  // Bases de données
  val clientsDB = List(
    (100, "John DOE"),
    (101, "Jane DOE"),
    (102, "Clark KENT")
  )

  val articlesDB = List(
    (100, "Lait"),
    (100, "Deodorant"),
    (101, "Crème du jour"),
    (101, "Book"),
    (101, "Lunettes"),
    (103, "Super PC")
  )

  // Initialisation de Spark
  val appName = "Jointures"
  val master = "local"
  val conf = new SparkConf().setAppName(appName).setMaster(master)
  val sc = new SparkContext(conf)
  sc.setLogLevel("ERROR")

  // Chargement des bases dans Spark
  val clients = sc.parallelize(clientsDB)
  val articles = sc.parallelize(articlesDB)

  // Exemples de jointures
  println("---------------")
  println("Inner Join")
  println("---------------")
  val join = clients.join(articles)
  join.collect().foreach(println)

  println("---------------")
  println("Right Join")
  println("---------------")
  val joinRight = clients.rightOuterJoin(articles)
  joinRight.collect().foreach(println)

  println("---------------")
  println("Left Join")
  println("---------------")
  val joinLeft = clients.leftOuterJoin(articles)
  joinLeft.collect().foreach(println)
}
