import java.io.File

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/* *********************************************************************************************************************
Objectif : compter le nombre d'occurences de chaque mot du fichier lines.txt

Solution :
(are,1)
(you,1)
(how,1)
(hello,3)
(world,2)
********************************************************************************************************************* */

object wordcount {
  def main(args: Array[String]): Unit = {

    // 1. Récupération du fichier de données
    val resource = getClass.getClassLoader.getResource("lines.txt")
    val filePath = new File(resource.toURI).getPath // chemin du fichier de données

    // 2. Configuration de Spark
    val conf: SparkConf = new SparkConf()
      .setMaster("local")
      .setAppName("wordcount")

    // 3. Création d'un SparkContext
    val sc: SparkContext = new SparkContext(conf)

    // 4. Chargement des lignes dans un RDD
    //val linesRDD = ???
    //linesRDD.collect().foreach(println)

    // 5. Split des lignes en mots
    //val wordsRDD = ???
    //wordsRDD.collect().foreach(println)

    // 6. Convertir les mots en minuscules
    //val words2RDD = ???
    //words2RDD.collect().foreach(println)

    // 7. Créer un RDD de paires clé-valeur du type (k,1)
    //val wordPairsRDD: RDD[(String, Int)] = ???
    //wordPairsRDD.collect().foreach(println)

    // 8. Calcul du nombre d'occurences de chaque mot
    //val numOccurences = ???
    //numOccurences.collect().foreach(println)
  }
}
